import { Component, OnInit } from '@angular/core';
import {Bolo} from '../modelos/bolo';
import {ActivatedRoute} from '@angular/router';
import {BolosService} from '../servicos/bolos.service';
import {LoadingController, ToastController} from '@ionic/angular';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {

  bolo: Bolo = {};
  id: number;
  loading: any;

  constructor(
      private loadingController: LoadingController,
      private toastController: ToastController,
      private activatedRoute: ActivatedRoute,
      private boloService: BolosService
  ) {
    this.id = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.getBolo();
  }

  getBolo(){
    this.bolo = this.boloService.getBolos()[this.id - 1];
  }

  async pagar() {
    await this.presentLoading();
    setTimeout(()=>{ this.loading.dismiss(); },1000);

  }


  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }




}
