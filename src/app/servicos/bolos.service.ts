import { Injectable } from '@angular/core';
import {Bolo} from '../modelos/bolo';

@Injectable({
  providedIn: 'root'
})
export class BolosService {
  constructor() { }
  getBolos (): Array<Bolo>{
    return [
      {
        id: 1,
        nome: "Bolo Normal",
        descricao: "Bolo muito bom",
        imagem: "../../assets/imagens/imagem1.jpeg",
        preco: 17
      },
      {
        id: 2,
        nome: "Bolo com cobertura",
        descricao: "Sensacional",
        imagem: "../../assets/imagens/imagem2.jpeg",
        preco: 50
      },
      {
        id: 3,
        nome: "Bolo de maça",
        descricao: "Maravilhoso",
        imagem: "../../assets/imagens/imagem3.jpeg",
        preco: 13
      },
      {
        id: 4,
        nome: "Bolo com morango",
        descricao: "#toop",
        imagem: "../../assets/imagens/imagem4.jpeg",
        preco: 8
      },
      {
        id: 5,
        nome: "Bolo de aniversário",
        descricao: "Este você não pode deixar passar",
        imagem: "../../assets/imagens/imagem5.jpeg",
        preco: 18
      },
      {
        id: 6,
        nome: "Bolo de uma coisa que não sei o que é",
        descricao: "Deve ser bom",
        imagem: "../../assets/imagens/imagem6.jpeg",
        preco: 25
      }
    ]
  }
}
